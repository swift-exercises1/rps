//
//  Sign.swift
//  RPS
//
//  Created by Sampada Sakpal on 16/8/20.
//  Copyright © 2020 Sampada Sakpal. All rights reserved.
//

import Foundation
import GameplayKit

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2);
 
func randomSign() -> Sign {
    let sign = randomChoice.nextInt()
    if(sign == 0) {
        return .rock
    } else if(sign == 1) {
        return .paper
    } else {
        return .scissors
    }
   
}
enum Sign {
    case rock, paper, scissors;
    var emoji: String {
        switch(self) {
        case .paper:
           return  "🤚";
        case .rock:
            return "👊";
        case .scissors:
            return "✌️";
        }
    }
    func compare(otherSign: Sign) -> GameState {
        if(self.emoji == otherSign.emoji) {
            return GameState.draw
        } else if (self.emoji == "🤚" && otherSign.emoji == "👊" || (self.emoji == "👊" && otherSign.emoji == "✌️") || (self.emoji ==  "✌️" && otherSign.emoji == "🤚" )) {
            return GameState.win
        }
        return GameState.lose
    }
}
