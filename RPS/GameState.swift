//
//  GameState.swift
//  RPS
//
//  Created by Sampada Sakpal on 16/8/20.
//  Copyright © 2020 Sampada Sakpal. All rights reserved.
//

import Foundation
enum GameState {
    case start, win, lose, draw;
}
