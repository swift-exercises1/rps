//
//  ViewController.swift
//  RPS
//
//  Created by Sampada Sakpal on 16/8/20.
//  Copyright © 2020 Sampada Sakpal. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.update(state: GameState.start)
   
    }
    
    func update(state: GameState) {
        if(state == GameState.draw) {
            self.status.text = "It's a draw!"
        } else if(state == GameState.lose) {
            self.status.text = "You lost!"
        } else if(state == GameState.win) {
            self.status.text = "You won!"
        } else {
            // hide play again button
            self.playAgain.isHidden = true
            // show all player sign buttons
            self.scissors.isEnabled = true
            self.paper.isEnabled = true
            self.rock.isEnabled = true
            // reset labels
            self.appSign.text = "🤖"
            self.status.text = "Rock, Paper, Scissors?"
        }
     
    }
    
    func play(sign: Sign) {
        let appTurn = randomSign()
        let gameState = sign.compare(otherSign: appTurn)
        self.update(state: gameState)
        self.appSign.text = appTurn.emoji
        // disable all player sign buttons
        self.scissors.isEnabled = false
        self.paper.isEnabled = false
        self.rock.isEnabled = false
        // hide all player sign buttons except the one player tapped
        if(sign.emoji == self.scissors.currentTitle) {
            self.paper.isHidden = true
            self.rock.isHidden = true
        } else if(sign.emoji == self.paper.currentTitle) {
            self.scissors.isHidden = true
            self.rock.isHidden = true
        } else {
            self.scissors.isHidden = true
            self.paper.isHidden = true
        }
        self.playAgain.isEnabled  = true
        self.playAgain.isHidden = false
    }

    @IBOutlet weak var appSign: UILabel!
    
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var rock: UIButton!
    
    @IBAction func rock(_ sender: Any) {
        self.play(sign: Sign.rock)
    }
    @IBOutlet weak var paper: UIButton!
    @IBAction func paper(_ sender: Any) {
        self.play(sign: Sign.paper)
    }
    @IBOutlet weak var scissors: UIButton!
    
    @IBAction func scissors(_ sender: Any) {
        self.play(sign: Sign.scissors)
    }
    @IBOutlet weak var playAgain: UIButton!
    @IBAction func playAgain(_ sender: Any) {
        self.update(state: GameState.start)
        self.scissors.isHidden = false
        self.paper.isHidden = false
        self.rock.isHidden = false
    }
    
}

